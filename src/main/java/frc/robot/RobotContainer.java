// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.DoubleSupplier;

import com.team6479.lib.controllers.CBJoystick;
import com.team6479.lib.controllers.CBXboxController;
import com.team6479.lib.util.DistanceCalculator;
import com.team6479.lib.util.dynamic.Limelight;
import com.team6479.lib.util.dynamic.Limelight.CamMode;
import com.team6479.lib.util.dynamic.Limelight.LEDState;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.autos.DriveOutTarmacAuto;
import frc.robot.autos.DriveShootTwoAuto;
import frc.robot.autos.ThreeBallAuto;
import frc.robot.commands.IndexAndShoot;
import frc.robot.commands.IntakeContinuously;
import frc.robot.commands.SpinUpFlywheelsClose;
import frc.robot.commands.SpinUpFlywheelsDeadReckon;
import frc.robot.commands.TeleopClimberTest;
import frc.robot.commands.TeleopDrive;
import frc.robot.commands.TeleopTurretControl;
import frc.robot.subsystems.ClimberSimple;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;
import frc.robot.subsystems.NavX;
import frc.robot.subsystems.Turret;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...

  private ArrayList<ArrayList<Double>> recording = null;
  private double timerOffset;
  private boolean justDidTeleop = false;
  private boolean hasBegunRecording = false;

  //private final IntakeArm intakeArm = new IntakeArm();
  private final IntakeArmSolenoid intakeArmSolenoid = new IntakeArmSolenoid();

  public static CBXboxController xbox =  new CBXboxController(0);
  public static final CBJoystick joystick = new CBJoystick(1);

  private DistanceCalculator dc;
  private DoubleSupplier calculatedDistance;

  private Drivetrain drivetrain = new Drivetrain();
  private Turret turret = new Turret(26, 280, 250);
  private Flywheels flywheels = new Flywheels(50, -1800);
  private Limelight limelight;
  private IntakeRollers intakeRollers = new IntakeRollers();
  private Indexer indexer = new Indexer();
  private ClimberSimple climber = new ClimberSimple();
  private Compressor compressor = new Compressor(PneumaticsModuleType.CTREPCM);
  // private Climber climber = new Climber();
  private NavX navX = new NavX();


  private boolean limelightOn;

  private SendableChooser<Command> autonChooser = new SendableChooser<Command>();

  private SendableChooser<Boolean> recordingChooser = new SendableChooser<Boolean>();

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer(Limelight limelight){
    this.limelight = limelight;

    // Configure the button bindings
    configureButtonBindings();

    this.dc = new DistanceCalculator(43.5, 104, (20*Math.PI)/180);
    this.calculatedDistance = () -> 10 - (dc.calculate(Math.toRadians(limelight.getYOffset()))%10) + dc.calculate(Math.toRadians(limelight.getYOffset()));

    // TODO: Recording auto code autonChooser.addOption("Recorded Auto Test", new NewRecording(drivetrain, indexer, intakeArmSolenoid, intakeRollers, limelight, flywheels, turret));
    autonChooser.addOption("Do Nothing", new InstantCommand());
    autonChooser.addOption("Drive shoot two", new DriveShootTwoAuto(drivetrain, navX, intakeArmSolenoid, limelight, turret, intakeRollers, indexer, flywheels, compressor));
    // autonChooser.addOption("Five Ball", new FiveBallAuto(drivetrain, navX, limelight, turret, intakeArmSolenoid, intakeRollers, indexer, flywheels));
    // autonChooser.addOption("Left Knock 2", new LeftSideKnock2BallAuto(drivetrain, navX, intakeArmSolenoid, limelight, turret, intakeRollers, flywheels, indexer));
    // autonChooser.addOption("Left Knock 2 Opposite", new LeftSideKnock2BallOppositeSideAuto(drivetrain, navX, intakeArmSolenoid, intakeRollers, limelight, turret, flywheels, indexer));
    // autonChooser.addOption("Right Knock 2", new RightSideKnock2BallAuto(drivetrain, navX, intakeArmSolenoid, limelight, turret, intakeRollers, flywheels, indexer));
    autonChooser.setDefaultOption("Drive out of tarmac", new DriveOutTarmacAuto(drivetrain, navX));
    // autonChooser.addOption("3 ball opp", new ThreeBallAutoOpp(drivetrain, navX, intakeArmSolenoid, limelight, turret, intakeRollers, indexer, flywheels));
    autonChooser.addOption("3 Ball auto", new ThreeBallAuto(drivetrain, navX, intakeArmSolenoid, limelight, turret, intakeRollers, indexer, flywheels, compressor));
    //autonChooser.setDefaultOption("Test SD", new StraightDrive(drivetrain, navX, -0.1, 40));
    limelightOn = false;
    Shuffleboard.getTab("SmartDashboard").add(autonChooser);

    recordingChooser.setDefaultOption("False", false);
    recordingChooser.addOption("True", true);
    Shuffleboard.getTab("SmartDashboard").add(recordingChooser);

    // CameraServer.startAutomaticCapture();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    DriverStation.reportWarning("configured buttons", false);

    // xbox.getButton(XboxController.Button.kRightBumper).whenPressed(new InstantCommand(() -> {
    //   indexer.setBottom(0.75);
    //   indexer.setTop(0.25);
    //   intakeRollers.rollersOn();
    //   intakeArmSolenoid.retract();
    // }, intakeRollers)).whenReleased(new InstantCommand(() -> {
    //   indexer.setBottom(0.0);
    //   indexer.setTop(0.0);
    //   intakeRollers.rollersOff();
    //   intakeArmSolenoid.push();
    // }, intakeRollers, intakeArmSolenoid));
    intakeArmSolenoid.setDefaultCommand(new IntakeContinuously(intakeArmSolenoid, intakeRollers, indexer, () -> xbox.getButton(Button.kRightBumper).get()));

    xbox.getButton(Button.kLeftBumper).whenPressed(new InstantCommand(() -> {
      if(intakeArmSolenoid.getPositon() == Value.kForward) {
        intakeArmSolenoid.push();
      } else {
        intakeArmSolenoid.retract();
      }
    }, intakeArmSolenoid));

    xbox.getButton(Button.kA).whenPressed(new InstantCommand(() -> {
      intakeRollers.rollersOn();
      indexer.setTop(0.45);
      indexer.setBottom(0.65);
    })).whenReleased(new InstantCommand(() -> {
      intakeRollers.rollersOff();
      indexer.setTop(0);
      indexer.setBottom(0.0);
    }));

    // intakeArm.setDefaultCommand(new TeleopIntakeArm(intakeArm, () -> xbox.getRightTriggerAxis() > 0, () -> xbox.getLeftTriggerAxis() > 0));

    // xbox.getButton(Button.kY)
    //   .whenPressed(new SequentialCommandGroup(
    //     new SpinUpFlywheels(flywheels, limelight),
    //     new WaitCommand(1),
    //     new InstantCommand(() -> indexer.setTop(0.0)),
    //     new WaitCommand(0.5),
    //     new InstantCommand(() -> indexer.setTop(.75), indexer),
    //     new InstantCommand(() -> indexer.setBottom(.75), indexer)
    //   )).whenReleased(new SequentialCommandGroup(
    //     new InstantCommand(flywheels::off, flywheels),
    //     new InstantCommand(() -> indexer.setTop(0.0), indexer),
    //     new InstantCommand(() -> indexer.setBottom(0.0), indexer)
    //   ));

    joystick.getButton(2)
      .whenPressed(new IndexAndShoot(limelight, drivetrain, indexer, flywheels, intakeRollers, compressor)).whenReleased(new SequentialCommandGroup(
        new InstantCommand(() -> flywheels.off(), flywheels),
        new InstantCommand(() -> indexer.setTop(0.0), indexer),
        new InstantCommand(() -> intakeRollers.rollersOff()),
        new InstantCommand(() -> indexer.setBottom(0.0), indexer)
      ));

    climber.setDefaultCommand(new TeleopClimberTest(climber, () -> joystick.getButton(7).get(), () -> joystick.getButton(8).get(), () -> joystick.getButton(11).get(), () -> joystick.getButton(12).get(), () -> joystick.getButton(10).get(), () -> joystick.getButton(9).get()));


    drivetrain.setDefaultCommand(
      new TeleopDrive(
        () -> xbox.getLeftY(),
        () -> -xbox.getRightX(),
        drivetrain
      )
    );

    // indexer.setDefaultCommand(new ColorLogger(indexer));

    joystick.getButton(3)
      .whenPressed(new InstantCommand(() -> {
        indexer.setBottom(-.65);
        indexer.setTop(-.5);
        intakeRollers.rollersReverse();
      })).whenReleased(new InstantCommand(() -> {
        indexer.setBottom(0);
        indexer.setTop(0);
        intakeRollers.rollersOff();
      }));

    // xbox.getButton(Button.kB)
    // SmartDashboard.putBoolean("Indexing", false);
    // joystick.getButton(6)
    //   .whenPressed(new SequentialCommandGroup(
    //     new InstantCommand(() -> {
    //       SmartDashboard.putBoolean("Indexing", true);
    //       indexer.setBottom(-.75);
    //       indexer.setTop(-0.75);
    //     }),
    //     new WaitCommand(.14),
    //     new InstantCommand(() -> {
    //       indexer.setBottom(0);
    //       indexer.setTop(0);
    //       SmartDashboard.putBoolean("Indexing", false);
    //     })
      // ));

    // xbox.getButton(Button.kY)
    joystick.getButton(4)
      .whenPressed(new InstantCommand(() -> {
        indexer.setBottom(.65);
        indexer.setTop(.3);
      }, indexer))
      .whenReleased(new InstantCommand(() -> {
        indexer.setBottom(0.0);
        indexer.setTop(0.0);
      }, indexer));


    /*
    new MultiButton(xbox, XboxController.Button.kStart.value, XboxController.Button.kBack.value)
      .whenPressed(
        new ConditionalCommand(
          new ConditionalCommand(
            new InstantCommand(),
            new ActuateClimber(climber),
            climber::getIsClimbing),
          new InstantCommand(() -> climber.setPosition(ArmPosition.OUT), climber),
        climber::hasExtended));
      */

      // new MultiButton(xbox, XboxController.Button.kStart.value, XboxController.Button.kBack.value)
      //   .whenPressed(
      //     new InstantCommand(() -> {
      //       if(climber.hasExtended()) {
      //         climber.setPosition(ArmPosition.IN);
      //       } else {
      //         climber.setPosition(ArmPosition.OUT);
      //       }
      //     })
      //   );

      joystick.getButton(1).whenPressed(new InstantCommand(() -> {
        if(limelightOn) {
          setLimelight(false);
        } else {
          setLimelight(true);
        }
      }));

      joystick.getButton(6)
      .whenPressed(new SequentialCommandGroup(
        new InstantCommand(() -> indexer.setBottom(0.6)),
        new InstantCommand(() -> indexer.setTop(0.6)),
        new WaitCommand(0.55),
        new InstantCommand(() -> indexer.setTop(0.0), indexer),
        new InstantCommand(() -> indexer.setBottom(0.0), indexer),
        new WaitCommand(0.1),
        new InstantCommand(() -> indexer.setBottom(-0.3)),
        new InstantCommand(() -> indexer.setTop(-0.5)),
        new WaitCommand(0.15),
        new InstantCommand(() -> indexer.setTop(0.0), indexer),
        new InstantCommand(() -> indexer.setBottom(0.0), indexer),
        new SpinUpFlywheelsDeadReckon(flywheels),
        new WaitCommand(0.5),
        new InstantCommand(() -> indexer.setTop(.35 + ((1 - RobotContainer.joystick.getRawAxis(3)) * 0.2)), indexer),
        new InstantCommand(() -> indexer.setBottom(.35 + ((1 - RobotContainer.joystick.getRawAxis(3)) * 0.2)), indexer),
        // new WaitCommand(0.5),
        // new InstantCommand(() -> indexer.setTop(0), indexer),
        // new InstantCommand(() -> indexer.setBottom(0), indexer),
        new WaitCommand(0.25)

      )).whenReleased(new SequentialCommandGroup(
        new InstantCommand(flywheels::off, flywheels),
        new InstantCommand(() -> indexer.setTop(0.0), indexer),
        new InstantCommand(() -> indexer.setBottom(0.0), indexer)
      ));

      joystick.getButton(5)
      // xbox.getButton(Button.kX)
      .whenPressed(new SequentialCommandGroup(
        new InstantCommand(() -> indexer.setBottom(0.6)),
        new InstantCommand(() -> indexer.setTop(0.6)),
        new WaitCommand(0.55),
        new InstantCommand(() -> indexer.setTop(0.0), indexer),
        new InstantCommand(() -> indexer.setBottom(0.0), indexer),
        new WaitCommand(0.1),
        new InstantCommand(() -> indexer.setBottom(-0.3)),
        new InstantCommand(() -> indexer.setTop(-0.5)),
        new WaitCommand(0.15),
        new InstantCommand(() -> indexer.setTop(0.0), indexer),
        new InstantCommand(() -> indexer.setBottom(0.0), indexer),
        new SpinUpFlywheelsClose(flywheels),
        new WaitCommand(0.5),
        new InstantCommand(() -> indexer.setTop(.35 + ((1 - RobotContainer.joystick.getRawAxis(3)) * 0.2)), indexer),
        new InstantCommand(() -> indexer.setBottom(.35 + ((1 - RobotContainer.joystick.getRawAxis(3)) * 0.2)), indexer),
        // new WaitCommand(0.5),
        // new InstantCommand(() -> indexer.setTop(0), indexer),
        // new InstantCommand(() -> indexer.setBottom(0), indexer),
        new WaitCommand(0.25)

      )).whenReleased(new SequentialCommandGroup(
        new InstantCommand(flywheels::off, flywheels),
        new InstantCommand(() -> indexer.setTop(0.0), indexer),
        new InstantCommand(() -> indexer.setBottom(0.0), indexer)
      ));

  }

  private void setLimelight(boolean on) {
    if(on) {
      limelightOn = true;
      limelight.setCamMode(CamMode.VisionProcessor);
      limelight.setLEDState(LEDState.Auto);
    } else {
      limelightOn = false;
      limelight.setCamMode(CamMode.DriverCamera);
      limelight.setLEDState(LEDState.Off);
    }
  }

  public void teleopInit() {
    recording = new ArrayList<ArrayList<Double>>();
    this.justDidTeleop = true;
    this.recording = new ArrayList<ArrayList<Double>>();
    if(recordingChooser.getSelected()) {
      timerOffset = Timer.getFPGATimestamp();
    }
    intakeArmSolenoid.push();
    turret.setDefaultCommand(new TeleopTurretControl(turret, limelight, () -> joystick.getZ(), () -> joystick.getButton(1).get()));
  }

  public void disabledInit() {
    indexer.setBottom(0.0);
    indexer.setTop(0.0);
    intakeRollers.rollersOff();
    turret.clearCorrection();
    flywheels.off();
    hasBegunRecording = false;
    if(recordingChooser.getSelected() && justDidTeleop) {
      justDidTeleop = false;
      String file = "package frc.robot.autos;import com.team6479.lib.util.dynamic.Limelight;import frc.robot.subsystems.Drivetrain;import frc.robot.subsystems.Flywheels;import frc.robot.subsystems.Indexer;import frc.robot.subsystems.IntakeArmSolenoid;import frc.robot.subsystems.IntakeRollers;import frc.robot.subsystems.Turret;public class NewRecording extends RecordedAuto {public NewRecording(Drivetrain drivetrain, Indexer indexer, IntakeArmSolenoid intakeArm, IntakeRollers intakeRollers, Limelight limelight, Flywheels flywheels, Turret turret) {super(drivetrain, indexer, intakeArm, intakeRollers, limelight, flywheels, turret);super.points = new double[][]{";
      for(ArrayList<Double> list : recording) {
        file += "{";
        for(double value : list) {
          file += value + ",";
        }
        file = file.substring(0,file.length()-1) + "},";
      }
      file = file.substring(0, file.length()-1) + "};}}";
      File f = new File("/home/lvuser/NewRecording.java");
      try {
        FileWriter fw = new FileWriter(f);
        fw.write(file);
        fw.close();
        DriverStation.reportError("Saved Auto to File /lvuser/NewRecording.java", false);
      } catch (IOException e) {
        e.printStackTrace();
      }
      recording = null;
    }
  }

  public void teleopPeriodic() {
    SmartDashboard.putNumber("Indexer override", .35 + ((1 - RobotContainer.joystick.getRawAxis(3)) * 0.2));
    if(limelight.hasTarget()) {
      SmartDashboard.putNumber("Distance to target", calculatedDistance.getAsDouble());
    }
    SmartDashboard.putNumber("NAVX YAW", navX.getYaw());
    if(recordingChooser.getSelected()) {
      double time = Timer.getFPGATimestamp() - timerOffset;
      double ly = xbox.getLeftY();
      double rx = xbox.getRightX();
      double rb = xbox.getRightBumper() ? 1 : 0;
      double b1 = joystick.getButton(1).get() ? 1 : 0;
      double b2 = joystick.getButton(2).get() ? 1 : 0;
      double yaw = joystick.getZ();
      // cut off the wait at the beginning
      if(!hasBegunRecording && ly == 0 && rx == 0) {
        timerOffset = Timer.getFPGATimestamp();
      } else {
        hasBegunRecording = true;
        ArrayList<Double> snapshot = new ArrayList<Double>();
        snapshot.add(time);
        snapshot.add(ly);
        snapshot.add(rx);
        snapshot.add(rb);
        snapshot.add(b1);
        snapshot.add(b2);
        snapshot.add(yaw);
        recording.add(snapshot);
      }
    }
  }

  public void autonomousPeriodic() {
    if(limelight.hasTarget()) {
      SmartDashboard.putNumber("Distance to target", calculatedDistance.getAsDouble());
    }
    SmartDashboard.putNumber("NAVX YAW", navX.getYaw());
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return autonChooser.getSelected();
  }
}
