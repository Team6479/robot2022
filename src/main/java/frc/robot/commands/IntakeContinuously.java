// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.BooleanSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;

public class IntakeContinuously extends CommandBase {

  private IntakeArmSolenoid intakeArmSolenoid;
  private IntakeRollers intakeRollers;
  private Indexer indexer;
  private BooleanSupplier isOn;
  private boolean hasStopped;

  /** Creates a new IntakeContinuously. */
  public IntakeContinuously(IntakeArmSolenoid intakeArmSolenoid, IntakeRollers intakeRollers, Indexer indexer, BooleanSupplier isOn) {
    this.intakeArmSolenoid = intakeArmSolenoid;
    this.intakeRollers = intakeRollers;
    this.indexer = indexer;
    this.isOn = isOn;
    this.hasStopped = false;
    addRequirements(intakeArmSolenoid, intakeRollers);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(isOn.getAsBoolean()) {
      hasStopped = false;
      indexer.setBottom(0.65);
      indexer.setTop(0.15);
      intakeRollers.rollersOn();
      intakeArmSolenoid.retract();
    } else if(!hasStopped) {
      hasStopped = true;
      indexer.setBottom(0.0);
      indexer.setTop(0.0);
      intakeRollers.rollersOff();
      intakeArmSolenoid.push();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
