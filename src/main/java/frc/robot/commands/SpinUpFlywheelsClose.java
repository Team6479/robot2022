// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Flywheels;

public class SpinUpFlywheelsClose extends CommandBase {

  private final double TOP_RPM = 150;
  private final double BOTTOM_RPM = 1700;

  private final double TOLERANCE = 50;

  private final Flywheels flywheels;
  public SpinUpFlywheelsClose(Flywheels flywheels) {
    this.flywheels = flywheels;
    addRequirements(this.flywheels);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    flywheels.setSpeed(TOP_RPM, BOTTOM_RPM);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return Math.abs(flywheels.getTopSpeed() - TOP_RPM) < TOLERANCE && Math.abs(flywheels.getBottomSpeed() - BOTTOM_RPM) < TOLERANCE;
  }
}
