// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.BooleanSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimberSimple;

public class TeleopClimberTest extends CommandBase {

  private ClimberSimple climber;
  private BooleanSupplier lb, lf, rb, rf, f, b;

  /** Creates a new TeleopClimberTest. */
  public TeleopClimberTest(ClimberSimple climber, BooleanSupplier lb, BooleanSupplier lf, BooleanSupplier rb, BooleanSupplier rf, BooleanSupplier f, BooleanSupplier b) {
    this.climber = climber;
    this.lb = lb;
    this.lf = lf;
    this.rb = rb;
    this.rf = rf;
    this.f = f;
    this.b = b;
    addRequirements(climber);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(lb.getAsBoolean()) {
      climber.setLeftRaw(-0.35);
      return;
    }
    if(lf.getAsBoolean()) {
      climber.setLeftRaw(0.35);
      return;
    }
    if(rb.getAsBoolean()) {
      climber.setRightRaw(-0.35);
      return;
    }
    if(rf.getAsBoolean()) {
      climber.setRightRaw(0.35);
      return;
    }
    if(f.getAsBoolean()) {
      climber.setRightRaw(0.35);
      climber.setLeftRaw(0.35);
      return;
    }
    if(b.getAsBoolean()) {
      climber.setRightRaw(-0.35);
      climber.setLeftRaw(-0.35);
      return;
    }
    climber.setLeftRaw(0);
    climber.setRightRaw(0);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
