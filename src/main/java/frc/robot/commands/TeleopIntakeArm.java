// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.BooleanSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeArm;

public class TeleopIntakeArm extends CommandBase {
  private final IntakeArm intakeArm;

  private final boolean outButton;
  private final boolean inButton;

  /** Creates a new TeleopIntakeArm. */
  public TeleopIntakeArm(IntakeArm intakeArm, BooleanSupplier outButton, BooleanSupplier inButton) {
    this.intakeArm = intakeArm;
    addRequirements(intakeArm);

    this.outButton = outButton.getAsBoolean();
    this.inButton = inButton.getAsBoolean();
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (outButton && !intakeArm.isAtFrontLimit()) {
      intakeArm.set(-0.25);
    }
    else if (inButton && !intakeArm.isAtBackLimit()) {
      intakeArm.set(0.25);
    }
    else {
      intakeArm.set(0);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    intakeArm.armStop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
