// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;


import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.NavX;

public class TurnDrivetrain extends CommandBase {

  private Drivetrain drivetrain;
  private NavX navX;
  private int angle;
  private long prevTime;
  private double prevPos;
  private double rate;

  private final int TOLERANCE = 8;

  /** Creates a new TurnDrivetrain. */
  public TurnDrivetrain(Drivetrain drivetrain, NavX navX, int angle) {
    this.drivetrain = drivetrain;
    this.navX = navX;
    this.angle = -MathUtil.clamp(angle, -160, 160);
    this.prevTime = System.currentTimeMillis();
    this.prevPos = 0;
    this.rate = 0;
    addRequirements(drivetrain, navX);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    this.navX.reset();
    this.prevPos = navX.getYaw();
    this.prevTime = System.currentTimeMillis();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // a bunch of jank p loops again, still works though
    if(Math.abs(angle) <= 45){
      SmartDashboard.putNumber("TD turn value", 0.012 * (this.navX.getYaw() + this.angle));
      drivetrain.arcadeDriveNoTurnCorrection(0, 0.012 * (this.navX.getYaw() + this.angle));
    } else if(Math.abs(angle) <= 90) {
      SmartDashboard.putNumber("TD turn value", 0.019 * (this.navX.getYaw() + this.angle));
      drivetrain.arcadeDriveNoTurnCorrection(0, 0.019 * (this.navX.getYaw() + this.angle));
    } else if(Math.abs(angle) <= 135) {
      SmartDashboard.putNumber("TD turn value", 0.015 * (this.navX.getYaw() + this.angle));
      drivetrain.arcadeDriveNoTurnCorrection(0, 0.015 * (this.navX.getYaw() + this.angle));
    } else {
      SmartDashboard.putNumber("TD turn value", 0.0045 * (this.navX.getYaw() + this.angle));
      drivetrain.arcadeDriveNoTurnCorrection(0, 0.0045 * (this.navX.getYaw() + this.angle));
    }

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    drivetrain.arcadeDrive(0, 0);
    DriverStation.reportWarning("td done", false);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    if(System.currentTimeMillis() - prevTime > 50) {
      rate = ((Math.abs(navX.getYaw()) - Math.abs(prevPos)))/((double)(System.currentTimeMillis()-prevTime));
      prevPos = navX.getYaw();
      prevTime = System.currentTimeMillis();
    }
    SmartDashboard.putNumber("turn roc", rate);
    SmartDashboard.putNumber("td error", Math.abs(Math.abs(navX.getYaw()) - Math.abs(this.angle)));
    return (Math.abs(Math.abs(navX.getYaw()) - Math.abs(this.angle)) < TOLERANCE && Math.abs(rate) < 0.1) || (Math.abs(rate) < 0.00001 && Math.abs(Math.abs(navX.getYaw()) - Math.abs(this.angle)) < this.angle/3);
  }
}
