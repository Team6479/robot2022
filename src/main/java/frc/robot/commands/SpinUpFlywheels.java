// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import com.team6479.lib.util.DistanceCalculator;
import com.team6479.lib.util.dynamic.Limelight;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Flywheels;

public class SpinUpFlywheels extends CommandBase {

  private final Flywheels flywheels;
  private final Limelight limelight;
  private DistanceCalculator distanceCalculator;
  private DoubleSupplier calculatedDistance;
  public SpinUpFlywheels(Flywheels flywheels, Limelight limelight) {
    this.flywheels = flywheels;
    this.limelight = limelight;
    distanceCalculator = new DistanceCalculator(43.5, 104, (20*Math.PI)/180); // height of limelight in inches, height of target in inches, angle of limelight in radians
    this.calculatedDistance = () -> 10 - (distanceCalculator.calculate(Math.toRadians(limelight.getYOffset()))%10) + distanceCalculator.calculate(Math.toRadians(limelight.getYOffset()));
    addRequirements(this.flywheels);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (limelight.hasTarget()) {
      // flywheels.setSpeed(SmartDashboard.getNumber("TS", 3100), SmartDashboard.getNumber("BS", 3100));
      // if(calculatedDistance.getAsDouble() < 110) {
      //   // flywheels.setSpeed(flywheels.getTopRPMClose(calculatedDistance.getAsDouble()), 3100); // TODO: make sure the RPM calculation is correct
      // } else {
      //   // flywheels.setSpeed(flywheels.getTopRPMFar(calculatedDistance.getAsDouble()), 3100); // TODO: make sure the RPM calculation is correct
      // }
      flywheels.setSpeed(flywheels.getTopSetpoint(calculatedDistance.getAsDouble()), flywheels.getBottomSetpoint());
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    SmartDashboard.putNumber("top RPM Finished", flywheels.getTopSpeed());
    SmartDashboard.putNumber("bottom RPM Finished", flywheels.getBottomSpeed());
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    SmartDashboard.putNumber("ft setpoint", flywheels.getTopSetpoint(calculatedDistance.getAsDouble()));
    return flywheels.isWithinTolerance(calculatedDistance.getAsDouble());
    // return flywheels.isWithinTolerance(calculatedDistance.getAsDouble());
  }
}
