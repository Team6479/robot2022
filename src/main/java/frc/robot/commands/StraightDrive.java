// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.NavX;

public class StraightDrive extends CommandBase {
  /** Creates a new StraightDrive. */
  private final Drivetrain drivetrain;
  private final NavX navX;
  private final double SPEED;
  private final double DISTANCE;
  private double navXStartPosition;
  private double previousVelocity;
  private double previousTime;
  private double sum;
  private double iteration;

  /**
   *
   * @param drivetrain Drivetrain instance
   * @param navX NavX instance
   * @param speed Speed (-1 to 1)
   * @param distance Distance in inches (must be positive)
   */
  public StraightDrive(Drivetrain drivetrain, NavX navX, double speed, double distance) {
    this.drivetrain = drivetrain;
    this.navX = navX;
    this.SPEED = speed;
    this.previousVelocity = 0;
    this.previousTime = 0;
    this.sum = 0;
    this.iteration = 0;
    // one wheel rotation = 5.74 motor rotations
    // 39.3701 = inches to meters
    this.DISTANCE = (distance/39.3701); // 5.0 is wheel diameter in inches
    // this.DISTANCE = 4.33*4096;
    addRequirements(drivetrain, navX);
  }

  @Override
  public void initialize() {
    DriverStation.reportError("SD Started", false);
    this.previousVelocity = navX.getVelocityY();
    this.previousTime = Timer.getFPGATimestamp();
    this.sum = 0;
    this.iteration = 0;
    this.navX.reset();
    this.navXStartPosition = navX.getYaw();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    iteration++;
    double velocity = navX.getVelocityY();
    double time = Timer.getFPGATimestamp();
    if(iteration%5==0) {
      double dtime = time - previousTime;
      sum += dtime * ((previousVelocity + velocity)/2);
    }
    previousVelocity = velocity;
    previousTime = time;
    // drivetrain.arcadeDrive(SPEED, -navX.getYaw() * 0.05);
    SmartDashboard.putNumber("SD NavX offset", Math.abs(navXStartPosition - navX.getYaw()));
    // jank p loop
    drivetrain.arcadeDriveNoTurnCorrection(-SPEED, -0.02 * (navXStartPosition - navX.getYaw()));
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    DriverStation.reportError("SD Done", false);
    drivetrain.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    SmartDashboard.putNumber("StraightDrive distance", sum);

    return Math.abs(sum) > DISTANCE;
  }

}
