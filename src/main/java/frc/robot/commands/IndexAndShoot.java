package frc.robot.commands;

import com.team6479.lib.util.dynamic.Limelight;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.RobotContainer;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeRollers;

public class IndexAndShoot extends SequentialCommandGroup {

  private Compressor compressor;

  public IndexAndShoot(Limelight limelight, Drivetrain drivetrain, Indexer indexer, Flywheels flywheels, IntakeRollers intakeRollers, Compressor compressor) {
    super(
      new InstantCommand(() -> compressor.disable()),
      new InstantCommand(() -> indexer.setBottom(0.6)),
      new InstantCommand(() -> indexer.setTop(0.6)),
      new WaitCommand(0.55),
      new InstantCommand(() -> indexer.setTop(0.0), indexer),
      new InstantCommand(() -> indexer.setBottom(0.0), indexer),
      new WaitCommand(0.1),
      new InstantCommand(() -> indexer.setBottom(-0.3)),
      new InstantCommand(() -> indexer.setTop(-0.5)),
      new WaitCommand(0.15),
      new InstantCommand(() -> indexer.setTop(0.0), indexer),
      new InstantCommand(() -> indexer.setBottom(0.0), indexer),
      new SpinUpFlywheels(flywheels, limelight),
      new WaitCommand(0.5),
      new InstantCommand(() -> indexer.setTop(.35 + ((1 - RobotContainer.joystick.getRawAxis(3)) * 0.2)), indexer),
      new InstantCommand(() -> indexer.setBottom(.35 + ((1 - RobotContainer.joystick.getRawAxis(3)) * 0.2)), indexer),
      new InstantCommand(() -> compressor.enableDigital())
    );
    this.compressor = compressor;
  }
  @Override
  public void end(boolean interrupted) {
    compressor.enableDigital();
  }
}
