// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.DrivetrainConstants;
import frc.robot.RobotContainer;

public class Drivetrain extends SubsystemBase {
  /** Creates a new Drivetrain. */

  private VictorSPX motorLeftFront;
  private VictorSPX motorLeftBack;
  private VictorSPX motorRightFront;
  private VictorSPX motorRightBack;

  private SlewRateLimiter rampForward, rampTurn;

  // TODO: one wheel rotation = 6.11 motor rotations
  public Drivetrain() {

    motorLeftFront = new VictorSPX(DrivetrainConstants.MOTOR_LEFT_FRONT);
    motorLeftBack = new VictorSPX(DrivetrainConstants.MOTOR_LEFT_BACK);
    motorRightFront = new VictorSPX(DrivetrainConstants.MOTOR_RIGHT_FRONT);
    motorRightBack = new VictorSPX(DrivetrainConstants.MOTOR_RIGHT_BACK);

    // Reset to factory defaults to ensure no config carryover
    motorLeftFront.configFactoryDefault();
    motorLeftBack.configFactoryDefault();
    motorRightFront.configFactoryDefault();
    motorRightBack.configFactoryDefault();

    // Have back motors follow front motors
    motorLeftBack.follow(motorLeftFront);
    motorRightBack.follow(motorRightFront);

    // Set the neutral mode
    motorLeftFront.setNeutralMode(NeutralMode.Brake);
    motorLeftBack.setNeutralMode(NeutralMode.Brake);
    motorRightFront.setNeutralMode(NeutralMode.Brake);
    motorRightBack.setNeutralMode(NeutralMode.Brake);
    
    // Invert Left Motors
    motorLeftFront.setInverted(true);
    motorLeftBack.setInverted(true);
    motorRightFront.setInverted(false);
    motorRightBack.setInverted(false);

    rampForward = new SlewRateLimiter(1.8);
    rampTurn = new SlewRateLimiter(3.0);
  }

  public void stop() {
    motorLeftFront.set(ControlMode.PercentOutput, 0);
    motorRightFront.set(ControlMode.PercentOutput, 0);
  }

  public void arcadeDrive(double forward, double turn) {
    forward = rampForward.calculate(MathUtil.clamp(forward, -1, 1)) / (RobotContainer.xbox.getRightTriggerAxis() > 0.25 ? 1 : 1.5);
    turn = rampTurn.calculate(MathUtil.clamp(turn ,-1, 1)) / (RobotContainer.xbox.getRightTriggerAxis() > 0.25 ? 1 : 1.5);
    motorLeftFront.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, +turn);
    motorRightFront.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -turn);
  }

  public void arcadeDriveNoTurnCorrection(double forward, double turn) {
    forward = rampForward.calculate(MathUtil.clamp(forward, -1, 1));
    motorLeftFront.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, +turn);
    motorRightFront.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -turn);
  }

}
