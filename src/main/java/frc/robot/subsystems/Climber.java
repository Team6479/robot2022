// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ClimberConstants;

public class Climber extends SubsystemBase {
  /** Creates a new Climber. */
  private DoubleSolenoid piston;
  private TalonFX motor;

  private boolean hasExtended;
  private boolean isClimbing;

  public enum ArmPosition {
    OUT, IN, MIDDLE
  }

  public Climber() {

    piston = new DoubleSolenoid(PneumaticsModuleType.REVPH, ClimberConstants.PISTON_FORWARD, ClimberConstants.PISTON_BACKWARD);
    motor = new TalonFX(ClimberConstants.LEFT_MOTOR);

    motor.configFactoryDefault();

    motor.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 30); // source: ctre examples

    motor.config_kP(0, 0.0);
    motor.config_kI(0, 0.0);
    motor.config_kD(0, 0.0);


    hasExtended = false;
  }

  public void setAngle(Value value){
    piston.set(value);
  }

  public boolean hasExtended() {
    return hasExtended;
  }

  public void setIsClimbing(boolean isClimbing) {
    this.isClimbing = isClimbing;
  }

  public boolean getIsClimbing() {
    return isClimbing;
  }

  public void setPosition(ArmPosition position){
    hasExtended = true;
    if(position == ArmPosition.OUT) {
      motor.set(ControlMode.Position, 0); // TODO: change me later
    } else if(position == ArmPosition.IN) {
      motor.set(ControlMode.Position, 0); // TODO: change me later
    } else if(position == ArmPosition.MIDDLE) {
      motor.set(ControlMode.Position, 0); // TODO: change me later
    }
  }

}
