// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.ControlType;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.SparkMaxPIDController;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.FlywheelConstants;

public class Flywheels extends SubsystemBase {

  private CANSparkMax bottomFlywheel;
  private SparkMaxPIDController bottomController;
  private CANSparkMax topFlywheel;
  private SparkMaxPIDController topController;
  private boolean isTopOn, isBottomOn;
  private double a1, b1;

  private final double RPM_TOLERANCE = 30;

  private final double BOTTOM_SETPOINT = 2800;

  public Flywheels(double a1, double b1) {

    bottomFlywheel = new CANSparkMax(FlywheelConstants.BOTTOM, MotorType.kBrushless);
    topFlywheel = new CANSparkMax(FlywheelConstants.TOP, MotorType.kBrushless);

    bottomFlywheel.restoreFactoryDefaults();
    topFlywheel.restoreFactoryDefaults();

    topController = topFlywheel.getPIDController();
    bottomController = bottomFlywheel.getPIDController();

    // PIDF tuning constants
    topController.setFF(.0001565); // changed
		topController.setP(0.00015); // changed
		topController.setI(0.0000001);
		topController.setD(0.0007);


    bottomController.setFF(.0001685); // changed
		bottomController.setP(0.00015);
		bottomController.setI(0.0000001);
		bottomController.setD(0.0007);

    // TOP NEW PIDF: 0.00007, 0.0000001, 0.0007, 0.000153
    // BOT NEW PIDF: 0.00015, 0.0000001, 0.0007, 0.000165

    bottomFlywheel.setInverted(true);

    isTopOn = false;
    isBottomOn = false;

    this.a1 = a1;
    this.b1 = b1;

    SmartDashboard.putNumber("Add Extra RPM", 0);
  }

  public void setTopFlywheelRawSpeed(double speed) {
    topFlywheel.set(speed);
    isTopOn = (speed != 0);
  }

  public void setBottomFlywheelRawSpeed(double speed) {
    bottomFlywheel.set(speed);
    isBottomOn = (speed != 0);
  }

  /**
   * Uses regression equation from real world testing to determine the top wheel rpm, along with distance to the target calculated with the limelight
   * @param distance Distance to the target
   */
  public double getTopSetpoint(double distance) {
    return (a1 * distance) + b1 + SmartDashboard.getNumber("Add Extra RPM", 0);
  }

  public double getBottomSetpoint() {
    return BOTTOM_SETPOINT;
  }

  /**
   * Sets the PIDF setpoint of the small flywheel
   * @param speed The rotational velocity in RPM
   */
  public void setTopFlywheelSpeed(double speed) {
    topController.setReference(speed, ControlType.kVelocity);
    isTopOn = (speed != 0);
  }

  /**
   * Sets the PIDF setpoint of the large flywheel
   * @param speed The rotational velocity in RPM
   */
  public void setBottomFlywheelSpeed(double speed) {
    bottomController.setReference(speed, ControlType.kVelocity);
    isBottomOn = (speed != 0);
  }

  public void off() {
    setTopFlywheelRawSpeed(0);
    isTopOn = false;
    setBottomFlywheelRawSpeed(0);
    isBottomOn = false;
  }

  public void setRawSpeed(double bottom, double top) {
    setBottomFlywheelRawSpeed(bottom);
    setTopFlywheelRawSpeed(top);
  }

  /**
   * Sets the PIDF setpoint of both flywheels
   * @param top The rotational velocity of the large flywheel in RPM
   * @param bottom The rotational velocity of the small flywheel in RPM
   */
  public void setSpeed(double top, double bottom) {
    setTopFlywheelSpeed(top);
    setBottomFlywheelSpeed(bottom);
  }

  /**
   * @param distance Distance from the target
   * @return true if the RPM of both wheels are within the tolerance
   */
  public boolean isWithinTolerance(double distance) {
    return Math.abs(getTopSpeed() - getTopSetpoint(distance)) < RPM_TOLERANCE && Math.abs(getBottomSpeed() - getBottomSetpoint()) < RPM_TOLERANCE;
    // if(distance < 110) {
    //   return Math.abs(getTopSpeed() - getTopRPMClose(distance)) < RPM_TOLERANCE;
    // } else {
    //   return Math.abs(getTopSpeed() - getTopRPMFar(distance)) < RPM_TOLERANCE;
    // }
    // boolean top = Math.abs(getTopSpeed() - SmartDashboard.getNumber("TS", 3100)) < RPM_TOLERANCE;
    // boolean bottom = Math.abs(getBottomSpeed() - SmartDashboard.getNumber("BS", 3100)) < RPM_TOLERANCE;
    // return top && bottom;
  }

  public double getRPMTolerance() {
    return RPM_TOLERANCE;
  }

  public double getTopSpeed() {
    return topFlywheel.getEncoder().getVelocity();
  }

  public double getBottomSpeed() {
    return bottomFlywheel.getEncoder().getVelocity();
  }

  public boolean getIsOn() {
    return isTopOn || isBottomOn;
  }

  @Override
  public void periodic() {
    // TODO Auto-generated method stub
    SmartDashboard.putNumber("T real speed", getTopSpeed());
    // DriverStation.reportError("T real speed " + getTopSpeed(), false);
    SmartDashboard.putNumber("B real speed", getBottomSpeed());
    // DriverStation.reportError("B real speed " + getBottomSpeed(), false);
  }
}
