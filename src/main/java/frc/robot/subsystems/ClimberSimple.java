// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ClimberConstants;

public class ClimberSimple extends SubsystemBase {
  /** Creates a new ClimberSimple. */
  private CANSparkMax climberLeft;
  private CANSparkMax climberRight;

  public ClimberSimple() {

    climberLeft = new CANSparkMax(ClimberConstants.LEFT_MOTOR, MotorType.kBrushless);
    climberRight = new CANSparkMax(ClimberConstants.RIGHT_MOTOR, MotorType.kBrushless);

    climberLeft.restoreFactoryDefaults();
    climberRight.restoreFactoryDefaults();

    climberLeft.setIdleMode(IdleMode.kBrake);
    climberRight.setIdleMode(IdleMode.kBrake);

  }

  public void periodic() {
    SmartDashboard.putNumber("Climber Left Pos", climberLeft.getEncoder().getPosition());
    SmartDashboard.putNumber("Climber right pos", climberRight.getEncoder().getPosition());
  }

  public void setLeftRaw(double raw) {
    climberLeft.set(raw);
  }

  public void setRightRaw(double raw) {
    climberRight.set(raw);
  }
}
