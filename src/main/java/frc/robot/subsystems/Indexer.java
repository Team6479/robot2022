// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IndexerConstants;

public class Indexer extends SubsystemBase {

  public enum BallColor {
    BLUE, RED, UNKNOWN
  }

  // private ColorSensorV3 colorSensor;
  // private ColorMatch colorMatcher;
  // private final Color blueTarget = new Color(0.24, 0.46, 0.31);
  // private final Color redTarget = new Color(0.46, 0.39, 0.16);

  private VictorSPX motorTop;
  private TalonSRX motorBottom;
  private boolean isTopOn, isBottomOn;

  /** Creates a new Indexer. */
  public Indexer() {
    motorTop = new VictorSPX(IndexerConstants.MOTOR_TOP);
    motorBottom = new TalonSRX(IndexerConstants.MOTOR_BOTTOM);
    isTopOn = false;
    isBottomOn = false;

    motorTop.configFactoryDefault();
    motorBottom.configFactoryDefault();

    motorTop.setNeutralMode(NeutralMode.Coast);
    motorBottom.setNeutralMode(NeutralMode.Brake);

    motorTop.setInverted(true);

    // this.colorSensor = new ColorSensorV3(I2C.Port.kOnboard);
    // colorMatcher = new ColorMatch();
    // colorMatcher.addColorMatch(blueTarget);
    // colorMatcher.addColorMatch(redTarget);
  }

  public void setBottom(double speed) {
    isBottomOn = speed == 0 ? false : true;
    motorBottom.set(ControlMode.PercentOutput, speed);
  }

  public void setTop(double speed) {
    isTopOn = speed == 0 ? false : true;
    motorTop.set(ControlMode.PercentOutput, speed);
  }

  public boolean isTopOn() {
    return isTopOn;
  }

  public boolean isBottomOn() {
    return isBottomOn;
  }

  // public void logColorValues() {
  //   SmartDashboard.putNumber("Red", colorSensor.getColor().red);
  //   SmartDashboard.putNumber("Green", colorSensor.getColor().green);
  //   SmartDashboard.putNumber("Blue", colorSensor.getColor().blue);
  //   ColorMatchResult match = colorMatcher.matchClosestColor(colorSensor.getColor());
  //   String colorString;

  //   if (match.color == blueTarget) {
  //     colorString = "Blue";
  //   } else if (match.color == redTarget) {
  //     colorString = "Red";
  //   } else {
  //     colorString = "Unknown";
  //   }

  //   SmartDashboard.putString("Color", colorString);
  //   SmartDashboard.putNumber("Confidence", match.confidence);
  // }

  // public BallColor getBallColor() {
  //   ColorMatchResult match = colorMatcher.matchClosestColor(colorSensor.getColor());
  //   if(match.color == blueTarget) {
  //     return BallColor.BLUE;
  //   } else if(match.color == redTarget) {
  //     return BallColor.RED;
  //   } else {
  //     return BallColor.UNKNOWN;
  //   }
  // }

}
