package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
//import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IntakeRollerConstants;


public class IntakeRollers extends SubsystemBase{
  private final VictorSPX intakeRoller = new VictorSPX(IntakeRollerConstants.INTAKE_ROLLER);

  public IntakeRollers() {
    intakeRoller.configFactoryDefault();

    intakeRoller.setInverted(false);
  }

  // Set the roller motors to on
  public void rollersOn() {
    intakeRoller.set(ControlMode.PercentOutput, .8);
  }

  // Set the roller motor to off
  public void rollersOff() {
    intakeRoller.set(ControlMode.PercentOutput, 0.0);
  }

  public void rollersReverse() {
    intakeRoller.set(ControlMode.PercentOutput, -.8);
  }

  public double getSpeed() {
    return intakeRoller.getMotorOutputPercent();
  }

  @Override
  public void periodic() {
    SmartDashboard.putBoolean("Are rollers on", Math.abs(getSpeed()) > 0);
  }
}
