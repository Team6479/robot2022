package frc.robot.autos;

import com.team6479.lib.util.dynamic.Limelight;
import com.team6479.lib.util.dynamic.Limelight.CamMode;
import com.team6479.lib.util.dynamic.Limelight.LEDState;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.commands.AimTurret;
import frc.robot.commands.SpinUpFlywheels;
import frc.robot.commands.StraightDrive;
import frc.robot.commands.TurnDrivetrain;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;
import frc.robot.subsystems.NavX;
import frc.robot.subsystems.Turret;

public class ThreeBallAutoOpp extends SequentialCommandGroup {
  public ThreeBallAutoOpp(Drivetrain drivetrain, NavX navX, IntakeArmSolenoid intakeArmSolenoid, Limelight limelight, Turret turret, IntakeRollers intakeRollers, Indexer indexer, Flywheels flywheels) {
    super(
      new InstantCommand(() -> intakeArmSolenoid.retract(), intakeArmSolenoid),
      new InstantCommand(() -> turret.setPosition(turret.getBackPos()), turret),
      new InstantCommand(() -> {
        intakeRollers.rollersOn();
        indexer.setBottom(0.75);
      }),
      new StraightDrive(drivetrain, navX, 0.35, 78), //Picks up the second ball
      new WaitCommand(0.25),
      new InstantCommand(() -> {
        indexer.setBottom(0);
        indexer.setTop(0);
        intakeRollers.rollersOff();
      }),
      //Would want to shoot here with the two balls
      new InstantCommand(() -> intakeArmSolenoid.push(), intakeArmSolenoid), //I would shoot first then bump the other ball away
      new WaitCommand(0.5),
      new TurnDrivetrain(drivetrain, navX, -110),
      new WaitCommand(0.25),
      new StraightDrive(drivetrain, navX, .35, 30),//Move with the 2 balls to second location to bump out ball
      new WaitCommand(0.25),
      new TurnDrivetrain(drivetrain, navX, 90),
      new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)), //Unless you know the angle exactly when hitting the other ball,
      new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)), //Try to hit it by going to 3rd same ball first, then coming back over line to hit it and shoot
      new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
      new WaitUntilCommand(() -> limelight.hasTarget()), // Only Aim and continue with shooting if we have a valid target
      new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
      new AimTurret(turret, limelight),
      new SpinUpFlywheels(flywheels, limelight),
      new InstantCommand(() -> {
        //limelight.setLEDState(LEDState.Off);
        //limelight.setCamMode(CamMode.DriverCamera);
      }),
      new InstantCommand(() -> {
        indexer.setBottom(0.75);
        indexer.setTop(0.75);
      }),
      new SequentialCommandGroup(
        new WaitCommand(1),
        new InstantCommand(() -> {
          indexer.setBottom(0);
          indexer.setTop(0);
          intakeRollers.rollersOff();
          flywheels.off();
        })
      ),
      //new TurnDrivetrain(drivetrain, navX, 110),
      new WaitCommand(0.5),
      //new InstantCommand(() -> turret.setPosition(turret.getBackPos()), turret),
      new TurnDrivetrain(drivetrain, navX, -140),
      new WaitCommand(0.5),
      new InstantCommand(() -> intakeArmSolenoid.retract(), intakeArmSolenoid),
      new InstantCommand(() -> {
        intakeRollers.rollersOn();
        indexer.setBottom(0.75);
        indexer.setTop(-0.5);
      }),
      new WaitCommand(0.25),
      new StraightDrive(drivetrain, navX, .4, 50),
      new WaitCommand(0.25),
      new InstantCommand(() -> {
        indexer.setBottom(0);
        indexer.setTop(0);
        intakeRollers.rollersOff();
      }),
      new TurnDrivetrain(drivetrain, navX, 100),
      new WaitCommand(0.5),
      new InstantCommand(() -> intakeArmSolenoid.push(), intakeArmSolenoid),
      new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
      new InstantCommand(() -> turret.setPosition(turret.getBackPos())),
      new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
      new WaitUntilCommand(() -> limelight.hasTarget()), // Only Aim and continue with shooting if we have a valid target
      new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
      new WaitCommand(0.5),
      new AimTurret(turret, limelight),
      new SpinUpFlywheels(flywheels, limelight),
      new InstantCommand(() -> {
        limelight.setLEDState(LEDState.Off);
        limelight.setCamMode(CamMode.DriverCamera);
      }),
      new InstantCommand(() -> {
        indexer.setBottom(0.75);
        indexer.setTop(0.75);
      }),
      new SequentialCommandGroup(
        new WaitCommand(3.0),
        new InstantCommand(() -> {
          indexer.setBottom(0);
          indexer.setTop(0);
          intakeRollers.rollersOff();
          flywheels.off();
        })
      )
    );
  }

}
