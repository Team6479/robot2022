package frc.robot.autos;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.StraightDrive;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.NavX;

public class DriveOutTarmacAuto extends SequentialCommandGroup {
    public DriveOutTarmacAuto(Drivetrain drivetrain, NavX navX) {
      super(
        new StraightDrive(drivetrain, navX, 0.5, 108) // Tarmac is 7ft 3/4 inches long. if robot is put anywhere in tarmac, then driving 108 inches will definitley get robot out of tarmac
      );
    }
}
