package frc.robot.autos;

import com.team6479.lib.util.dynamic.Limelight;

import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;
import frc.robot.subsystems.Turret;

public class TemplateRecordedAuto extends RecordedAuto {
    public TemplateRecordedAuto(Drivetrain drivetrain, Indexer indexer, IntakeArmSolenoid intakeArm, IntakeRollers intakeRollers, Limelight limelight, Flywheels flywheels, Turret turret) {
        super(drivetrain, indexer, intakeArm, intakeRollers, limelight, flywheels, turret);
        super.points = new double[][]{
            // timestamp, lefty, rightx, rightbumper, button1, button2, joystickyaw
            {0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0}
        };
    }
}
