package frc.robot.autos;

import com.team6479.lib.util.dynamic.Limelight;
import com.team6479.lib.util.dynamic.Limelight.CamMode;
import com.team6479.lib.util.dynamic.Limelight.LEDState;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.commands.AimTurret;
import frc.robot.commands.SpinUpFlywheels;
import frc.robot.commands.StraightDrive;
import frc.robot.commands.TurnDrivetrain;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;
import frc.robot.subsystems.NavX;
import frc.robot.subsystems.Turret;

public class FiveBallAuto extends SequentialCommandGroup {
  public FiveBallAuto(Drivetrain drivetrain, NavX navX, Limelight limelight, Turret turret, IntakeArmSolenoid intakeArmSolenoid, IntakeRollers intakeRollers, Indexer indexer, Flywheels flywheels) {
    super(
      new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
      new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
      new WaitUntilCommand(limelight::hasTarget), // Only Aim and continue with shooting if we have a valid target
      new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
      new InstantCommand(() -> turret.setPosition(turret.getCenter()), turret),
      new AimTurret(turret, limelight),
      new SpinUpFlywheels(flywheels, limelight),
      new InstantCommand(() -> indexer.setTop(1.0), indexer),
      new InstantCommand(() -> indexer.setBottom(1.0), indexer),
      new WaitCommand(2.0), // TODO: Tweak value
      new InstantCommand(() -> limelight.setLEDState(LEDState.Off)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.DriverCamera)),
      new SpinUpFlywheels(flywheels, limelight),
      new InstantCommand(() -> indexer.setTop(0.0), indexer),
      new InstantCommand(() -> indexer.setBottom(0.0), indexer),
      new TurnDrivetrain(drivetrain, navX, 160), // TODO: Change angle
      new InstantCommand(() -> intakeArmSolenoid.push(), intakeArmSolenoid),
      new InstantCommand(() -> intakeRollers.rollersOn(), intakeRollers),
      new StraightDrive(drivetrain, navX, 0.5, 40), // TODO: Change distance
      new TurnDrivetrain(drivetrain, navX, 120), // TODO: Change degrees
      new StraightDrive(drivetrain, navX, 0.5, 60),
      new InstantCommand(() -> intakeRollers.rollersOff(), intakeRollers),
      new TurnDrivetrain(drivetrain, navX, 100), // TODO: Change Angle
      new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
      new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
      new WaitUntilCommand(limelight::hasTarget), // Only Aim and continue with shooting if we have a valid target
      new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
      new InstantCommand(() -> turret.setPosition(turret.getCenter()), turret),
      new AimTurret(turret, limelight),
      new SpinUpFlywheels(flywheels, limelight),
      new InstantCommand(() -> indexer.setTop(1.0), indexer),
      new InstantCommand(() -> indexer.setBottom(1.0), indexer),
      new WaitCommand(2.0), // TODO: Tweak value
      new InstantCommand(() -> limelight.setLEDState(LEDState.Off)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.DriverCamera)),
      new InstantCommand(() -> flywheels.off()),
      new InstantCommand(() -> indexer.setTop(0.0), indexer),
      new InstantCommand(() -> indexer.setBottom(0.0), indexer),
      new InstantCommand(() -> intakeRollers.rollersOff()),
      new TurnDrivetrain(drivetrain, navX, -50), // TODO: Change angle
      new InstantCommand(() -> intakeRollers.rollersOn(), intakeRollers),
      new StraightDrive(drivetrain, navX, 0.5, 120), // TODO: Change distance
      new InstantCommand(() -> intakeRollers.rollersOff(), intakeRollers),
      new TurnDrivetrain(drivetrain, navX, 300), // TODO: Change angle
      new StraightDrive(drivetrain, navX, 0.5, 120), // TODO: Change distance
      new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
      new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
      new WaitUntilCommand(limelight::hasTarget), // Only Aim and continue with shooting if we have a valid target
      new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
      new InstantCommand(() -> turret.setPosition(turret.getCenter()), turret),
      new AimTurret(turret, limelight),
      new SpinUpFlywheels(flywheels, limelight),
      new InstantCommand(() -> indexer.setTop(1.0), indexer),
      new InstantCommand(() -> indexer.setBottom(1.0), indexer)
    );
  }
}
