// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.autos;

import java.util.function.DoubleSupplier;

import com.team6479.lib.util.DistanceCalculator;
import com.team6479.lib.util.dynamic.Limelight;
import com.team6479.lib.util.dynamic.Limelight.CamMode;
import com.team6479.lib.util.dynamic.Limelight.LEDState;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;
import frc.robot.subsystems.Turret;

public abstract class RecordedAuto extends CommandBase {

  private Drivetrain drivetrain;
  private Indexer indexer;
  private IntakeArmSolenoid intakeArm;
  private IntakeRollers intakeRollers;
  private Limelight limelight;
  private Flywheels flywheels;
  private Turret turret;

  private DistanceCalculator distanceCalculator;
  private DoubleSupplier calculatedDistance;

  private Timer one, two, three, four = new Timer();

  protected double[][] points;
  private int index;
  private double startTime;
  private boolean limelightOn;
  private boolean shouldFinishShooting;
  private boolean shouldTurnOffIntake;
  private boolean canToggleLimelight;
  private boolean visionDelayState = false;


  /** Creates a new RecordedAuto. */
  public RecordedAuto(Drivetrain drivetrain, Indexer indexer, IntakeArmSolenoid intakeArm, IntakeRollers intakeRollers, Limelight limelight, Flywheels flywheels, Turret turret) {
    this.drivetrain = drivetrain;
    this.indexer = indexer;
    this.intakeArm = intakeArm;
    this.intakeRollers = intakeRollers;
    this.limelight = limelight;
    this.flywheels = flywheels;
    this.turret = turret;
    // This should be overridden in subclasses
    this.one = new Timer();
    this.two = new Timer();
    this.three = new Timer();
    this.four = new Timer();
    this.points = new double[0][0];
    this.index = 0;
    this.startTime = 0;
    this.limelightOn = false;
    this.shouldTurnOffIntake = false;
    this.shouldFinishShooting = false;
    this.canToggleLimelight = true;
    distanceCalculator = new DistanceCalculator(43.5, 104, (20*Math.PI)/180); // height of limelight in inches, height of target in inches, angle of limelight in radians
    this.calculatedDistance = () -> 10 - (distanceCalculator.calculate(Math.toRadians(limelight.getYOffset()))%10) + distanceCalculator.calculate(Math.toRadians(limelight.getYOffset()));
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(drivetrain);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    index = 0;
    drivetrain.arcadeDrive(0, 0);
    limelight.setCamMode(CamMode.DriverCamera);
    limelight.setLEDState(LEDState.Off);
    turret.setPosition(turret.getBackPos());
    one.stop();
    one.reset();
    two.stop();
    two.reset();
    three.stop();
    three.reset();
    four.stop();
    four.reset();
    this.limelightOn = false;
    this.shouldFinishShooting = false;
    this.shouldTurnOffIntake = false;
    this.canToggleLimelight = true;
    this.startTime = Timer.getFPGATimestamp();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if((points[index][0]) < (Timer.getFPGATimestamp() - startTime) && index < points.length-1) {
      SmartDashboard.putNumber("Index", index);
      // drive
      drivetrain.arcadeDrive(points[index][1], -points[index][2]);

      // intake
      if(points[index][3] != 0) { // if button is pressed
        shouldTurnOffIntake = true;
        indexer.setBottom(0.65);
        indexer.setTop(0.15);
        intakeRollers.rollersOn();
        intakeArm.retract();
      } else if(points[index][3] == 0 && shouldTurnOffIntake){
        shouldTurnOffIntake = false;
        indexer.setBottom(0.0);
        indexer.setTop(0.0);
        intakeRollers.rollersOff();
        intakeArm.push();
      }

      // limelight
      if(points[index][4] != 0 && canToggleLimelight) { // if pressed
        canToggleLimelight = false;
        if(limelightOn) {
          limelight.setLEDState(LEDState.Auto);
          limelight.setCamMode(CamMode.VisionProcessor);
        } else {
          limelight.setLEDState(LEDState.Off);
          limelight.setCamMode(CamMode.DriverCamera);
        }
        limelightOn = !limelightOn;
      } else {
        canToggleLimelight = true;
      }

      // shoot
      if(points[index][5] != 0) {
        this.shouldFinishShooting = true;
        // one hasn't started
        if(one.get() == 0) {
          indexer.setBottom(0.6);
          indexer.setTop(0.6);
          DriverStation.reportError("Start 1", false);
          one.start();
        }
        // two hasn't started and one is done
        if(two.get() == 0 && one.get() >= 0.55) {
          indexer.setBottom(0.0);
          indexer.setTop(0.0);
          DriverStation.reportError("Start 2", false);
          two.start();
        }
        // three hasn't started and first 2 are done
        if(three.get() == 0 && one.get() >= 0.55 && two.get() >= 0.1) {
          indexer.setBottom(-0.3);
          indexer.setTop(-0.5);
          DriverStation.reportError("Start 3", false);
          three.start();
        }
        // four hasn't started and the first 3 are done
        if(four.get() == 0 && one.get() >= 0.55 && two.get() >= 0.1 && three.get() >= 0.15) {
          if(limelight.hasTarget()) {
            indexer.setBottom(0.0);
            indexer.setTop(0.0);
            DriverStation.reportError("LL Has Target", false);
            flywheels.setSpeed(flywheels.getTopSetpoint(calculatedDistance.getAsDouble()), flywheels.getBottomSetpoint());
          }
        }
        // start 4 when flywheels reach speed
        if(four.get() == 0 && flywheels.isWithinTolerance(calculatedDistance.getAsDouble())) {
          DriverStation.reportError("Start 4", false);
          four.start();
        }
        // after 4 is done
        if(one.get() >= 0.55 && two.get() >= 0.1 && three.get() >= 0.15 && four.get() >= 0.5) {
          indexer.setBottom(0.35);
          indexer.setTop(0.35);
          intakeRollers.rollersReverse();
        }
      }
      if(points[index][5] == 0 && this.shouldFinishShooting) {
        this.shouldFinishShooting = false;
        indexer.setTop(0);
        indexer.setBottom(0);
        intakeRollers.rollersOff();
        flywheels.off();
        one.stop();
        one.reset();
        two.stop();
        two.reset();
        three.stop();
        three.reset();
        four.stop();
        four.reset();
      }

      // turret
      boolean isCorrected = turret.isCorrected();
      boolean hasTarget = limelight.hasTarget();
      boolean isOverridden = points[index][5] != 0;

      // We want target searching to begin before correction completes
      if (!isCorrected && !hasTarget && visionDelayState) {
        visionDelayState = false;
      }

      if (hasTarget && (isCorrected || !visionDelayState) && !isOverridden) {
        if (!isCorrected) {
          turret.clearCorrection();
        }
        turret.setPosition(turret.getCurrentAngle() + (limelight.getXOffset()));
        if (turret.isCorrected()) {
          visionDelayState = true;
        }
      } else if (isCorrected || isOverridden) {
        double joystickValue = points[index][6];
        turret.setPercentOutput(Math.abs(joystickValue) > 0.4 ? joystickValue : 0);
      }

      index++;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    drivetrain.arcadeDrive(0, 0);
    DriverStation.reportError("finished", false);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return index >= points.length-1;
  }
}
