package frc.robot.autos;

import com.team6479.lib.util.dynamic.Limelight;
import com.team6479.lib.util.dynamic.Limelight.CamMode;
import com.team6479.lib.util.dynamic.Limelight.LEDState;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.commands.AimTurret;
import frc.robot.commands.IndexAndShoot;
import frc.robot.commands.StraightDrive;
import frc.robot.commands.TurnDrivetrain;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;
import frc.robot.subsystems.NavX;
import frc.robot.subsystems.Turret;

public class ThreeBallAuto extends SequentialCommandGroup {
  public ThreeBallAuto(Drivetrain drivetrain, NavX navX, IntakeArmSolenoid intakeArmSolenoid, Limelight limelight, Turret turret, IntakeRollers intakeRollers, Indexer indexer, Flywheels flywheels, Compressor compressor) {
    super(
      new InstantCommand(() -> intakeArmSolenoid.retract(), intakeArmSolenoid),
      new InstantCommand(() -> turret.setPosition(turret.getBackPos()), turret),
      new InstantCommand(() -> {
        intakeRollers.rollersOn();
        indexer.setBottom(0.75);
        indexer.setTop(0.25);
      }),
      new StraightDrive(drivetrain, navX, 0.25, 32),
      new WaitCommand(.35),
      new InstantCommand(() -> {
        indexer.setBottom(0);
        indexer.setTop(0);
        intakeRollers.rollersOff();
      }),
      new InstantCommand(() -> intakeArmSolenoid.push(), intakeArmSolenoid),
      new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
      new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
      new WaitUntilCommand(() -> limelight.hasTarget()), // Only Aim and continue with shooting if we have a valid target
      new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
      new AimTurret(turret, limelight),
      new IndexAndShoot(limelight, drivetrain, indexer, flywheels, intakeRollers, compressor),
      new InstantCommand(() -> {
        limelight.setLEDState(LEDState.Off);
        limelight.setCamMode(CamMode.DriverCamera);
      }),
      new SequentialCommandGroup(
        new WaitCommand(1.15),
        new InstantCommand(() -> {
          indexer.setBottom(0);
          indexer.setTop(0);
          intakeRollers.rollersOff();
          flywheels.off();
        })
      ),
      new WaitCommand(0.25),
      new TurnDrivetrain(drivetrain, navX, 95),
      new WaitCommand(0.25),
      new InstantCommand(() -> intakeArmSolenoid.retract(), intakeArmSolenoid),
      new InstantCommand(() -> {
        intakeRollers.rollersOn();
        indexer.setBottom(0.75);
        indexer.setTop(-0.5);
      }),
      new StraightDrive(drivetrain, navX, .33, 117),
      new WaitCommand(0.25),
      new InstantCommand(() -> {
        indexer.setBottom(0);
        indexer.setTop(0);
        intakeRollers.rollersOff();
      }),
      new TurnDrivetrain(drivetrain, navX, -45),
      new InstantCommand(() -> intakeArmSolenoid.push(), intakeArmSolenoid),
      new StraightDrive(drivetrain, navX, -0.33, 30),
      new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
      new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
      new WaitUntilCommand(() -> limelight.hasTarget()), // Only Aim and continue with shooting if we have a valid target
      new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
      new AimTurret(turret, limelight),
      new IndexAndShoot(limelight, drivetrain, indexer, flywheels, intakeRollers, compressor),
      new InstantCommand(() -> {
        limelight.setLEDState(LEDState.Off);
        limelight.setCamMode(CamMode.DriverCamera);
      }),
      new SequentialCommandGroup(
        new WaitCommand(3.0),
        new InstantCommand(() -> {
          indexer.setBottom(0);
          indexer.setTop(0);
          intakeRollers.rollersOff();
          flywheels.off();
        })
      )
    );
  }

}
